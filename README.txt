Web Proxy Assignment 

Name:	Scott Wells
Class:	CS4480 Computer Networks
Date:	February 20, 2015

HOW TO COMPILE AND RUN PROXY ON CADE MACHINES:

Make sure that you are in a directory that holds the 'proxy' folder in it (not in the proxy folder) with a terminal.
Run the following command to compile:
	
	"javac proxy/*.java"

	OR

	"javac proxy/Run.java proxy/Response.java proxy/Request.java proxy/CachedResponse.java"

Once it completes, run the proxy using the following:

	"java proxy/Run 7000"

You may replace 7000 with whatever argument you would like to use as the port number the proxy server will run on.
This has been compiled and tested with JRE 1.7 and JRE 1.6 that the CADE machines use.

CACHED FILES INFORMATION:

The 'cache' folder storing all of the cache files will be located in the 'proxy' folder alongside all of the source .java and 
.class files. The proxy creates subdirectories within the 'cache' folder that are titled by what the host name was. In these
folders there could be many subdirectories (depending on what site was fetched) containing various files that contain the site's
data. The files contained will have the same extensions that were fetched.

Example: http://www.example.com/hi.html
	will cache the file 'hi.html' in directory 'proxy/cache/www.example.com/'

When a website is cached that has NO file extention, a default html file is created in the cache.

Example: http://www.cs.utah.edu
	will cache the file 'default.html' in directory 'proxy/cache/www.cs.utah.edu/'

TESTED WEBSITES (Working):

http://www.w3.org
http://www.cs.utah.edu
http://www.reddit.com

ADDITIONAL NOTES:
2/20/2015
	Added Features:
	-Proxy now has website caching capabilities
		-The proxy will fetch and store sites that are requested from clients. If a client visits a site that has been previously 
		loaded during the current proxy's run-time, this proxy will deliver the cached copy of the site located on the machine's hard disk.
		-Cached sites are not remembered to be in the cache by the proxy between proxy shutdowns. However, the data fetched from previous
		runs will remain on disk until they are deleted. If a site is requested and it has previous cache data, that data will be over-
		written with the response from the newer request.

	Other Notes:
	-Code was reorganized in Run to minimize the Run class logic from previous versions. It makes more sense now.
	-CachedResponse class was added to add capability of sending file read from disk through a socket to the connected client.
	-Response class now saves the site to disk and simultaneously delivers it to the client.

2/7/2015
	Added Features:
	-Added multithreading for service to simultaneous connections from multiple clients.
	-Sends 501 error message to client if any request other than GET is sent to the proxy.
	-Added exception handling to keep proxy running when clients close while the connection is still open (waiting for input).
	-Changed the proxy sending mechanism. It now sends pure data (bytes) to and from the target host, instead of only strings
		like it was before. This makes it so media like photos and video may be sent through the proxy just fine.

	This proxy has been tested with Firefox and works as expected for most (but not all) websites. Sites that require any other
	requests other than GET will not work correctly!

1/30/2015
	This proxy application has only very basic functionality and is buggy. If the client closes the terminal
	running telnet before giving the request, the proxy will crash. However, the proxy does support HTTP GET requests
	formatted as the following:
		1)	GET <FULL URI> <VERSION>

		3)	GET <RELATIVE URI> <VERSION>
			Host: <HOSTNAME>

	The proxy will return the web page as specified in part A of the assignment, however there is no guarantee on the
	functionality when given incorrectly formatted GET requests. This and other safe functionality checks will be handled
	in the following weeks before the final version is complete. 
		
