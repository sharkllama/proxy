package proxy;
import java.io.*;
import java.net.Socket;
/**
 * A Response object represents the communication between the proxy and the remote web server.
 * It sends a message to the server and listens for a response. Once it has received the response,
 * The response object sends what was transmitted from the remote server to the client and simultaneously stores
 * the website on disk for a faster response back to the client if that or another client requests
 * that same website.
 * 
 * @author 	Scott Wells
 * @version	February 20, 2015
 */
public class Response {
	
	// Stored message to send back to the client later
	private byte[] backToClient;
	private int bytesRead;
	
	// File path of the file written to disk from the response from the server. 
	private String cachedFilePath;
	
	/**
	 * Constructor uses the given DataOutputStream and BufferedReader to send information to and from
	 * the web server. Using the Request object given, sends the outgoing message back to the client.
	 * @param out
	 * @param in
	 * @param request
	 * @throws IOException 
	 */
	public Response(Socket clientSocket, Request request) throws IOException{
		// Initialize all Dataoutput and input streams
		Socket outToRemoteServer = new Socket(request.getHost(), request.getPort());
		DataOutputStream out = new DataOutputStream(outToRemoteServer.getOutputStream());
		DataInputStream in = new DataInputStream(outToRemoteServer.getInputStream());
		DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
		
		// Format outgoing message
		String outMessage = "GET " + request.getURI() + " " + request.getVersion() + "\r\n" +
							"Host: " + request.getHost() + "\r\n" + "Connection: close\r\n\r\n";
		//Send the message
		out.writeBytes(outMessage);
			
		//debug message (uncomment to see outgoing message to the web server from the proxy.
		//System.out.println("Sending " + outMessage);
		
		byte[] buffer = new byte[2048];
		int bytesRead = 0;
		// Store the file path this response will save the fetched message to.
		this.cachedFilePath = "./proxy/cache/" + request.getHost() + request.getURI();
		this.cachedFilePath = this.cachedFilePath.substring(0, this.cachedFilePath.lastIndexOf('/'));
		File f = new File(this.cachedFilePath);
		f.mkdirs();
		
		// if no file extension specified by the request, a default html file is created to hold the site data
		this.cachedFilePath += request.getURI().substring(request.getURI().lastIndexOf('/'), request.getURI().length());
		if(request.getURI().equals("/"))
			f = new File(this.cachedFilePath += "/default.html");
		else
			f = new File(this.cachedFilePath);
		
		
		// Build the message being received and write the response to a file on disk
		try {
			FileOutputStream writeToCache = new FileOutputStream(f);
			while((bytesRead = in.read(buffer)) != -1)
			{
				// Send the data to the client while also saving the response on disk
				outToClient.write(buffer, 0, bytesRead);
				writeToCache.write(buffer, 0, bytesRead);
				outToClient.flush();
				writeToCache.flush();
			}
			writeToCache.close(); // close up fileWriter
			outToRemoteServer.close();
		} catch (IOException e) {
			System.out.println("Problem occured when reading response from remote server.");
		}
		
		// Full message received. clean up.
		try {
			in.close();
			out.close();
			outToClient.close();
		} catch (IOException e) {
			System.out.println("Error closing reader from response.");
		}
		// debug print statement (uncomment to see what the proxy will be forwarding back to the client)
		// System.out.println("Outgoing message to client: " + builder.toString());
		System.out.println("Response delivered and file cached successfully.");
		backToClient = buffer;
		this.bytesRead = bytesRead;
	}
	/**
	 * Returns the completed response message from the remote web server of this response.
	 * @return
	 */
	public byte[] getClientMessage(){return backToClient;}
	public String getCachedFilePath(){return cachedFilePath;}
	public int getBytesRead(){return this.bytesRead;}
}
