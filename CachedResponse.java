package proxy;
import java.io.*;
import java.net.Socket;
/**
 * A cached response object sends the stored website (on disk) given the filepath. The File is sent using the given
 * socket. The socket is assumed to already be connected to the client. The filepath is also assumed to have a valid path 
 * to a file on disk to deliver.
 * 
 * @author 	Scott Wells
 * @version	February 20, 2015
 *
 */
public class CachedResponse {
	
	/**
	 * Constructor sends the file given with the file path to the socket.
	 * @param clientSocket - already connected DataOutputStream object
	 * @param filepath - path to the file stored on disk to send
	 * @throws IOException 
	 */
	public CachedResponse(Socket clientSocket, String filepath) throws IOException{
			DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
			File f = new File(filepath);
			FileInputStream in = null;
		
			byte[] buffer = new byte[2048];
			int bytesRead = 0;
			// Build the message being received
			try {
				in = new FileInputStream(f);
				while((bytesRead = in.read(buffer)) != -1)
				{
					outToClient.write(buffer, 0, bytesRead);
					outToClient.flush();
				}
				System.out.println("Cached response sent successfully");
			} catch (IOException e) {
				System.out.println("Problem occured when reading response from cached website on disk.");
			}
			
			// Full message sent. clean up.
			try {
				if(in != null)
					in.close();
				outToClient.close();
			} catch (IOException e) {
				System.out.println("Error closing reader from file reader.");
			}
		}
	}

