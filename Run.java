package proxy;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * The Run class initializes and runs a HTTP proxy server to listen on the port specified in the command line argument.
 * Clients can connect to the proxy and supply an HTTP GET request to make the proxy fetch the webpage specified and
 * send it back to the client.
 * 
 * @author 	Scott Wells
 * @version February 20, 2015
 */
public class Run implements Runnable
{
	private static int portNum;
	private Socket clientSocket;
	
	// cache hash table maps the site name to the file path associated with the name
	private static Hashtable<String, String> cache;
	
	/**
	 * Run constructor saves exactly one client socket (for use with threads)
	 * @param clientSocket
	 */
	public Run(Socket clientSocket){
		this.clientSocket = clientSocket;
	}
	
	/**
	 * Runs the proxy server on the given port argument from the command line.
	 * @param args - args[0] supplies port number
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException{
		if(args.length != 1){
			System.out.println("Error. must give one argument to give a port to use for the proxy!");
			return;
		}
		
		portNum = Integer.parseInt(args[0]);
		ServerSocket listener;
		
		// set the total amount of threads to 100. Threadpool will manage and reuse threads as needed.
		Executor threadPool = Executors.newFixedThreadPool(100);
		
		// initialize the cache (hashtable mapping URI to File path).
		cache = new Hashtable<String, String>();
		
		// try to open a socket using the specified port number
		try {
			listener = new ServerSocket(portNum);
		} catch (IOException e1) {
			System.out.println("The specified port argument could not be used. Program terminated.");
			return;
		}
		
		// open up a socket to use as a listening connection.
		try{
			System.out.println("Proxy is now running. Listening for connections");
			while(true){
				Socket socket = listener.accept();
				System.out.println("Connection recieved from: " + socket.getInetAddress().toString());
				
				// listen on the established connection for a GET request from the client
				threadPool.execute(new Run(socket));
			}
		}catch(IOException e){}
		finally{
			// close the port so that the system doesn't get clogged
			listener.close();
		}
			
	}
	
	/**
	 * This method creates a Request object to handle the incoming transmission from the client socket.
	 * @throws IOException 
	 */
	public void listenForHTTPRequest() throws IOException{
		Request request = null;
		try{
			request = new Request(clientSocket);
			
			// This sends the 501 error to the client if the client sent anything other than a GET request
			if(!request.validRequest()){
				DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
				outToClient.writeBytes(request.getVersion() + " 501 Not Implemented\r\n");
				outToClient.close();
				clientSocket.close();
				return;
			}	
		}
		catch(Exception e){
			System.out.println("Unexpected error with socket from " + this.clientSocket.getInetAddress().toString() + ", closing connection.");
			this.clientSocket.close();
			return;
			}
		
		// after the request has been parsed, contact the web server
		respondToClient(request);
	}
	
	/**
	 * This method attempts to open a connection to the remote server using the information given from the
	 * client GET request. Once the connection has been made, a Response object is created to communicate
	 * between the proxy, remote server, and the client.
	 * 
	 * @param request - current request being executed
	 * @throws IOException 
	 */
	public void respondToClient(Request request) throws IOException{
		try{
			// If the site is already cached, send a cached response to the client
			if(cache.containsKey(request.getHost() + request.getURI() + request.getPort())){
				CachedResponse response = new CachedResponse(clientSocket, cache.get(request.getHost() + request.getURI() + request.getPort()));
			}
			else{
			// Otherwise, the site is fetched and cached for the next request for that site
				Response response = new Response(clientSocket, request);
				cache.put(request.getHost() + request.getURI() + request.getPort(), response.getCachedFilePath());
			}
		// Close up the socket either way (from error or working normally)
		}catch(Exception e){
			System.out.println("Error closing socket or processing response to " + this.clientSocket.getInetAddress().toString());
			this.clientSocket.close();
			return;
		}
		System.out.println("Socket closed");
		this.clientSocket.close();
	}
	
	/**
	 * run method for the multiple threads. this method is called right after the thread is created to call
	 * it's listenForHTTPRequest to handle that connection
	 */
	@Override
	public void run() {
		try {
			this.listenForHTTPRequest();
		} catch (IOException e) {
			return;
		}
	}
}
