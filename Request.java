package proxy;
import java.io.*;
import java.net.Socket;
/**
 * A request object represents a GET request made by a client to the proxy.
 * 
 * @author 	Scott Wells
 * @version February 20, 2015
 */
public class Request {
	
	private String uri;
	private String host;
	private int port = 80;
	private String request;
	private String version;
	private boolean validRequest;
	
	/**
	 * Constructor takes in a Buffered reader to listen for incoming data from the client. Once data has been
	 * transmitted over the line, the request parses the text received to connect to the remote web server.
	 * @param in - incoming messages from the client
	 * @throws IOException
	 */
	public Request(Socket clientSocket) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		validRequest = true;
		String line = in.readLine();
		String[] lineArr = line.split(" ");
		
		if(lineArr.length < 3){
			System.out.println("Invalid Request received");
			return;
		}
		
		version = lineArr[2];
		uri = lineArr[1];
		
		request = lineArr[0]; // This should be GET!!
		
		if(!request.equals("GET")){
			System.out.println("Invalid Request recieved.");
			validRequest = false;
			return;
		}
		version = lineArr[2];
		uri = lineArr[1];
		//check the uri segment to see if it's a full uri
		boolean fullURI = uri.startsWith("http://");
		
		// break apart the string if the whole path is in the first line
		if(fullURI){
			int endHostIndex = uri.indexOf("/", 7);
			
			// uri line does not have a slash after the host
			if(endHostIndex != -1){
				host = uri.substring(7, endHostIndex);
				uri = uri.substring(endHostIndex, uri.length());
				
				// custom port information parse
				int URIportNumIndex = uri.indexOf(":");
				if(URIportNumIndex != -1){
					port = Integer.parseInt(uri.substring(URIportNumIndex + 1, uri.length()));
					uri = uri.substring(0, URIportNumIndex);
				}
			}
			// uri does have a forward slash after the host
			else{
				host = uri.substring(7, uri.length());
				uri = "/";
				
				// find custom port information if it exists
				int URIportNumIndex = host.indexOf(":");
				if(URIportNumIndex != -1){
					port = Integer.parseInt(host.substring(URIportNumIndex + 1, host.length()));
					host = host.substring(0, URIportNumIndex);
				}
			}	
		}
		//not a full URI (host only) i.e. www.cs.utah.edu
		else{
			//grab the next line to get host information
			line = in.readLine();
			lineArr = line.split(" ");
			host = lineArr[1];
			
			// get port information if specified in the URL
			int URIportNumIndex = host.indexOf(":");
			if(URIportNumIndex != -1){
				port = Integer.parseInt(host.substring(URIportNumIndex + 1, host.length()));
				host = host.substring(0, URIportNumIndex);
			}
		}
		System.out.println("Request URI: " + uri + " Host: " + host + " Port: " + port);	
	}
	
	/**
	 * Returns the requested URI.
	 * @return
	 */
	public String getURI(){return uri;}
	/**
	 * Returns the requested port to use on the web server (80 by default)
	 * @return
	 */
	public int getPort(){return port;}
	/**
	 * Returns the host information.
	 * @return
	 */
	public String getHost(){return host;}
	/**
	 * Returns the HTTP Request information (GET, REMOVE, etc.)
	 * @return
	 */
	public String getRequest(){return request;}
	/**
	 * Returns the version information.
	 * @return
	 */
	public String getVersion(){return version;}
	/**
	 * Returns true if the request is a valid request that this proxy supports.
	 * @return
	 */
	public boolean validRequest(){return validRequest;}
}

